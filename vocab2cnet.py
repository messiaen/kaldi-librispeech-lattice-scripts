import sys
import os


def read_stm_times(stm):
    times = {}
    for line in filter(lambda l: len(l) > 0, stm):
        if line.startswith(';;'):
            continue
        fields = line.strip().split()
        audio_id = fields[0]
        channel = fields[1]
        start = float(fields[3])
        end = float(fields[4])
        words = fields[6:]

        times[audio_id] = (start, end)

    return times



def convert(times, v_audio_id, infile, outfile):
    edges = []
    for i, word in enumerate(map(str.strip, infile)):
        edges.append('J={0}\tS=0\tE=1\tW={1}\tv=0.000000\ta=0.0\tl=0.0\ts=1.0'.format(i, word))
    version = 'VERSION=1.1\n'
    uttr = 'UTTERANCE={}\n'.format(v_audio_id)
    info = 'N=2\tL={}\n'.format(len(edges))
    n1 = 'I=0\tt={}\n'.format(times[0])
    n2 = 'I=1\tt={}\n'.format(times[1])

    outfile.write(version)
    outfile.write(uttr)
    outfile.write(info)
    outfile.write(n1)
    outfile.write(n2)
    outfile.write('\n'.join(edges))


def main():
    stm_fn = sys.argv[1]
    indir = sys.argv[2]
    outdir = sys.argv[3]

    with open(stm_fn, 'r', encoding='utf-8') as f:
        times = read_stm_times(f)

    for fn in os.listdir(indir):
        if os.path.isfile(os.path.join(indir, fn)):
            v_audio_id = fn
            if fn.endswith('.lat'):
                v_audio_id = fn.split('.')[0]

            with open(os.path.join(indir, fn), 'r', encoding='utf-8') as inf:
                with open(os.path.join(outdir, fn), 'w', encoding='utf-8') as outf:
                    convert(times[v_audio_id], v_audio_id, inf, outf)


if __name__ == "__main__":
    main()
