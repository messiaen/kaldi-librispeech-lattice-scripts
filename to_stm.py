import sys
import os


datadir = sys.argv[1]


utt2spk_filename=os.path.join(datadir, 'utt2spk')
utt2dur_filename=os.path.join(datadir, 'utt2dur')
text_filename=os.path.join(datadir, 'text')


utt2spk = {}
with open(utt2spk_filename, 'r') as f:
    for line in f:
        utt_id, spk_id = line.strip().split()
        utt2spk[utt_id] = spk_id


utt2dur = {}
with open(utt2dur_filename, 'r') as f:
    for line in f:
        utt_id, dur = line.strip().split()
        utt2dur[utt_id] = dur


text = {}
with open(text_filename, 'r') as f:
    for line in f:
        utt_id, utt = line.strip().split(' ', 1)
        text[utt_id] = utt


stm_line = '{0} {1} {2} {3} {4} <{5}> {6}'


for utt_id, utt in text.items():
    audio_id = utt_id
    channel = 'A'
    speaker = utt2spk.get(utt_id, 'NULL')
    start_time = 0.0
    stop_time = utt2dur.get(utt_id, None)
    if not stop_time:
        continue
    print(stm_line.format(
        audio_id,
        channel,
        speaker,
        start_time,
        stop_time,
        'NULL',
        utt))
