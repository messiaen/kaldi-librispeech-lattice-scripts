#!/bin/bash

KALDI_HOME=/home/eigenmoose/workspace/kaldi-trunk

LATTCP=$KALDI_HOME/src/latbin/lattice-copy

SOURCE_DIR=$KALDI_HOME/egs/librispeech/s5/exp/tri3b/decode_nosp_tgsmall_test_clean

OUTPUT_DIR=./slfs

INT2SYM=$KALDI_HOME/egs/librispeech/s5/utils/int2sym.pl

WORDS=$KALDI_HOME/egs/librispeech/s5/data/lang_nosp_test_tgsmall/words.txt

CONV_SLF=$KALDI_HOME/egs/librispeech/s5/utils/convert_slf.pl

mkdir -p $OUTPUT_DIR
for LATFILE in $SOURCE_DIR/lat.*.gz; do
	$LATTCP "ark:zcat $LATFILE |" ark,t:- | $INT2SYM -f 3 $WORDS > /tmp/tmp.lat
	$CONV_SLF /tmp/tmp.lat $OUTPUT_DIR
	rm -f /tmp/tmp.lat
done

