import sys
import os


def read_stm_times(stm):
    times = {}
    for line in filter(lambda l: len(l) > 0, stm):
        if line.startswith(';;'):
            continue
        fields = line.strip().split()
        audio_id = fields[0]
        channel = fields[1]
        start = float(fields[3])
        end = float(fields[4])
        words = fields[6:]

        times[audio_id] = (start, end)

    return times



def convert(times, v_audio_id, infile, outfile):
    for word in map(str.strip, infile):
        outfile.write('{0} {1} {2} {3} {4}\n'.format(
            v_audio_id,
            'A',
            times[0],
            times[1] - times[0],
            word))


def main():
    stm_fn = sys.argv[1]
    indir = sys.argv[2]
    outdir = sys.argv[3]

    with open(stm_fn, 'r', encoding='utf-8') as f:
        times = read_stm_times(f)

    for fn in os.listdir(indir):
        if os.path.isfile(os.path.join(indir, fn)):
            v_audio_id = fn
            if fn.endswith('.lat'):
                v_audio_id = fn.split('.')[0]

            with open(os.path.join(indir, fn), 'r', encoding='utf-8') as inf:
                with open(os.path.join(outdir, fn), 'w', encoding='utf-8') as outf:
                    convert(times[v_audio_id], v_audio_id, inf, outf)


if __name__ == "__main__":
    main()
